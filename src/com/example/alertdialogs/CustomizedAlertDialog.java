package com.example.alertdialogs;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class CustomizedAlertDialog extends Activity
{
Button b1,b2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customizeddialog);
		this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		b1=(Button)findViewById(R.id.button1);
		b2=(Button)findViewById(R.id.button2);
		b1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			Toast.makeText(CustomizedAlertDialog.this,"You Cliked Yes Button",Toast.LENGTH_SHORT).show();	
			
			}
		});
		b2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(CustomizedAlertDialog.this,"You Cliked No Button",Toast.LENGTH_SHORT).show();		
			}
		});
				}

	
}
