package com.example.alertdialogs;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button b1, b2, b3, b4;
	String[] os = { "Windows", "Mac", "Ubuntu", "Fedora", "RedHat", "Unix",
			"Sun OS", "Solarsis", "Debain", "Open Mint", "Kali Linux",
			"Zorin os", "Open SUSE", "Elementary OS" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		b1 = (Button) findViewById(R.id.button1);
		b2 = (Button) findViewById(R.id.button2);
		b3 = (Button) findViewById(R.id.button3);
		b4 = (Button) findViewById(R.id.button4);

		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder alert = new AlertDialog.Builder(
						MainActivity.this);

				alert.setTitle("Verified Works");
				alert.setIcon(R.drawable.canvas);
				alert.setCancelable(false);
				alert.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Toast.makeText(getApplicationContext(),
										"You Clicked Yes Button",
										Toast.LENGTH_LONG).show();
							}
						});
				alert.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Toast.makeText(getApplicationContext(),
										"Dialog cancelled", Toast.LENGTH_SHORT)
										.show();
								dialog.cancel();
							}
						});
				alert.show();

			}
		});
		b2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent i = new Intent(MainActivity.this,
						CustomizedAlertDialog.class);
				startActivity(i);
			}
		});
		b3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog d = new Dialog(MainActivity.this);
				d.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
				d.setContentView(R.layout.customdialog1);
				d.getWindow().setBackgroundDrawableResource(
						android.R.color.black);
				d.setCancelable(false);
				TextView app_name = (TextView) d.findViewById(R.id.textView1);
				Typeface font = Typeface.createFromAsset(getAssets(),
						"alba.TTF");
				app_name.setTypeface(font);
				Button submit = (Button) d.findViewById(R.id.submit);
				Button cancel = (Button) d.findViewById(R.id.close);
				submit.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Toast.makeText(MainActivity.this,
								"Clicked Submit Button", Toast.LENGTH_SHORT)
								.show();
					}
				});
				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						d.cancel();
						Toast.makeText(MainActivity.this,
								"Clicked Close Button", Toast.LENGTH_SHORT)
								.show();
					}
				});
				d.show();
			}
		});
		b4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Dialog d1 = new Dialog(MainActivity.this);
				d1.setContentView(R.layout.customdialog2);
				d1.setCancelable(true);
				SearchView view = (SearchView) d1
						.findViewById(R.id.search_view);
				final ListView listview = (ListView) d1
						.findViewById(R.id.LView);
				ArrayAdapter<String> aa=new ArrayAdapter<String>(MainActivity.this,
						android.R.layout.simple_list_item_1, os);
				listview.setAdapter(aa);
				listview.setTextFilterEnabled(false);
				view.setIconifiedByDefault(false);
				view.setQueryHint("Search The Query");
				final android.widget.Filter filter=aa.getFilter();
				view.setOnQueryTextListener(new OnQueryTextListener() {

					@Override
					public boolean onQueryTextSubmit(String query) {
						// TODO Auto-generated method stub
						return false;
					}

					@Override
					public boolean onQueryTextChange(String newText) {
						// TODO Auto-generated method stub
						if (TextUtils.isEmpty(newText)) {
							listview.clearTextFilter();
						} else {
							filter.filter(newText);
						}
						return true;
					}
				});
				d1.show();
			}
		});
	}

}
